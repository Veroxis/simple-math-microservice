use features::Features;
use rocket::get;
use rocket::launch;
use rocket::routes;

mod features;

static mut UUID: uuid::Uuid = uuid::Uuid::nil();

#[get("/")]
async fn home() -> String {
    format!("UUID: {}\n{:#?}", unsafe { UUID }, Features::new())
}

#[get("/add/<num_a>/<num_b>")]
async fn add(num_a: f64, num_b: f64) -> String {
    (num_a + num_b).to_string()
}

#[get("/sub/<num_a>/<num_b>")]
async fn sub(num_a: f64, num_b: f64) -> String {
    (num_a - num_b).to_string()
}

#[get("/mul/<num_a>/<num_b>")]
async fn mul(num_a: f64, num_b: f64) -> String {
    (num_a * num_b).to_string()
}

#[get("/div/<num_a>/<num_b>")]
async fn div(num_a: f64, num_b: f64) -> String {
    (num_a / num_b).to_string()
}

#[launch]
async fn rocket() -> _ {
    unsafe { UUID = uuid::Uuid::new_v4() };
    let mut math_routes = routes![home];
    let features = Features::new();

    if features.add {
        math_routes.extend(routes![add]);
    }

    if features.sub {
        math_routes.extend(routes![sub]);
    }

    if features.mul {
        math_routes.extend(routes![mul]);
    }

    if features.div {
        math_routes.extend(routes![div]);
    }

    rocket::build().mount("/", math_routes)
}
