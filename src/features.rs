use std::env;

#[derive(Debug, Default)]
pub struct Features {
    pub add: bool,
    pub sub: bool,
    pub mul: bool,
    pub div: bool,
}

impl Features {
    pub fn new() -> Self {
        Self {
            add: Self::is_enabled("FEAT_ADD"),
            sub: Self::is_enabled("FEAT_SUB"),
            mul: Self::is_enabled("FEAT_MUL"),
            div: Self::is_enabled("FEAT_DIV"),
        }
    }

    fn is_enabled(env_key: &str) -> bool {
        if let Ok(env_var) = env::var(env_key) {
            if env_var.as_str().trim().is_empty() || env_var.as_str() == "0" || env_var.as_str() == "false" {
                return false;
            }
        }
        true
    }
}
