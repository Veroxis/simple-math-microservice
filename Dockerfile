FROM alpine:latest

ENV ROCKET_ADDRESS=0.0.0.0
ENV ROCKET_ENV=production

COPY target/x86_64-unknown-linux-musl/release/simple-math-microservice /simple-math-microservice

ENTRYPOINT ["/simple-math-microservice"]
